-- copyright 2019 quinn
-- available for use under the GPLv3 or later, see LICENSE
{-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}
import System.Environment
import Control.Monad
import GHC.Generics
import qualified System.Directory as D
import Control.Exception (catch, IOException)
import Data.Void (Void)
import Text.Megaparsec as M
import Text.Megaparsec.Char as M
import qualified Data.Aeson as A
import qualified Data.Aeson.Types as A
import qualified Data.HashMap.Strict as HM
import Data.ByteString.Lazy (readFile)
import qualified System.FilePath.Find as F
import System.FilePath

configFile :: FilePath
configFile = "config.json"

type Parser = Parsec Void String

parseFile f = do
  i <- Prelude.readFile f
  x <- readConf configFile
  case parse (anyParser x) f i of
    Left s -> error $ show s
    Right x -> return $ concat x

anyParser :: Conf -> Parser [String]
anyParser x = M.many $ keyvParser x

keyvParser :: Conf -> Parser String
keyvParser x =
  (try (replaceKey x)) <|> mySingle

replaceKey :: Conf -> Parser String
replaceKey x = do
  _ <- char '#'
  _ <- char '{'
  key <- some $ alphaNumChar
  _ <- char '}'
  return $ readKey key x

mySingle :: Parser String
mySingle = do
  s <- anySingle
  return [s]

type Conf = (HM.HashMap String String)

readConf :: FilePath -> IO Conf
readConf f = do
  x <- Data.ByteString.Lazy.readFile f
  case A.eitherDecode x :: Either String Conf of
    Left a -> error $ show a
    Right a -> return a

readKey :: String -> Conf -> String
readKey y x =
  case HM.lookup y x of
    Just a -> a
    Nothing -> error $ "key \"" ++ y ++ "\" was used, but not present in dictionary"

fcTrue :: F.FindClause Bool
fcTrue = return True

fcFile :: F.FindClause Bool
fcFile = F.fileType F.==? F.RegularFile

fcDetermine y = do
  x <- F.find fcTrue fcFile y
  return $ map (fcRemovePrefix y) x

fcRemovePrefix a b =
  joinPath $ removeNFromHead (length (splitPath a)) (splitPath b)

removeNFromHead :: Int -> [a] -> [a]
removeNFromHead 0 y = y
removeNFromHead x (y:ys) =
  removeNFromHead (x - 1) ys

parseAndWrite :: FilePath -> FilePath -> FilePath -> IO ()
parseAndWrite a b c = do
  x <- parseFile $ a </> c
  fcWriteFile (b </> c) x

fcWriteFile :: FilePath -> String -> IO ()
fcWriteFile a b =
  D.createDirectoryIfMissing True (takeDirectory a) >> writeFile a b

doAll :: FilePath -> FilePath -> IO ()
doAll a b = do
  x <- fcDetermine a
  void $ mapM (parseAndWrite a b) x

main = do
  args <- getArgs
  doAll (args !! 0) (args !! 1)
